# Installation
These instructions assume that you're using a Windows machine. I apologize to my Linux and OSX brethren. 

###  Install Python and Venv (I'm not your mom figure it out)
    After that's done...

### Make a virtual environment
    python -m venv venv

### Activate said virtual environment
    .\venv\Scripts\activate


### Assign a local environment variable for Flask to use
    $env:FLASK_APP = "main.py"
### OR if you're using CMD instead of Powershell
    set FLASK_APP=main.py

### Now run
    flask run

Setup was relatively easy and the only hard part was getting the images hooked up properly to the HTML template. The main.py code is extremely minimal and effective.